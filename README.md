PHP Yourls Client
=================

API Client for a Yourls instance.

Easy and simple to use interface to manage your short urls from a php application.

This library is in a very early stage of production. 

Requirements
------------

 * php >= 7.4 || ^8.0


Installation
------------

```bash
composer require cross-solution/php-yourls-client
```

Configuration
-------------

TBD

Licence
-------

MIT
